using System;
using System.Collections.Generic;

namespace UploadServer
{
    public class UploadServerConfig
    {
        /// <summary>
        /// 网站根目录
        /// </summary>
        public string RootUrl { get; set; }

        /// <summary>
        /// 上传地址
        /// </summary>
        public string EntryPoint1 { get; set; }

        /// <summary>
        /// 上传地址
        /// </summary>
        public string EntryPoint2 { get; set; }

        /// <summary>
        /// 上传文件的虚拟根目录
        /// </summary>
        public string VirtualPath { get; set; }

        /// <summary>
        /// 上传文件的物理根目录
        /// </summary>
        public string PhysicalPath { get; set; }

        /// <summary>
        /// 增加的mime类型映射，使用;号隔开
        /// </summary>
        public string AppendMimes { get; set; }

        /// <summary>
        /// 输出缓存秒数
        /// </summary>
        public long ResponseCache { get; set; }

        /// <summary>
        /// jwt的秘钥
        /// </summary>
        public string JWTSecret { get; set; }

        /// <summary>
        /// 默认的可以上传文件后缀，;号隔开
        /// </summary>
        public string AllowExts { get; set; }

        /// <summary>
        /// 默认限制大小,支持mb和kb
        /// </summary>
        public string LimitSize { get; set; }

        private long byteSize = 10 * 1024 * 1024; //默认10m

        public long GetByteSize()
        {
            if (!string.IsNullOrEmpty(LimitSize))
            {
                if (LimitSize.Length > 2)
                {
                    var strNum = LimitSize.Substring(0, LimitSize.Length - 2);

                    if (long.TryParse(strNum, out var num))
                    {
                        if (LimitSize.EndsWith("kb", StringComparison.OrdinalIgnoreCase))
                        {
                            byteSize = num * 1024;
                        }
                        else if (LimitSize.EndsWith("mb", StringComparison.OrdinalIgnoreCase))
                        {
                            byteSize = num * 1024 * 1024;
                        }
                    }
                }
            }

            return byteSize;
        }

        /// <summary>
        /// 各个app的配置
        /// </summary>
        public Dictionary<string, AppConfig> Apps { get; set; }


        public AppConfig GetAppConfig(string appName)
        {
            var defConfig = Apps["default"];
            var appConfig = new AppConfig();
            if (Apps.TryGetValue(appName, out AppConfig spConfig))
            {
                appConfig.AllowOrigins = spConfig.AllowOrigins ?? defConfig.AllowOrigins ?? string.Empty;
                appConfig.EnableThumbnail = spConfig.EnableThumbnail ?? defConfig.EnableThumbnail;
                appConfig.ThumbnailExts = spConfig.ThumbnailExts ?? defConfig.ThumbnailExts ?? string.Empty;
                appConfig.LimitExts = spConfig.LimitExts ?? defConfig.LimitExts ?? string.Empty;
            }
            else
            {
                appConfig.AllowOrigins = defConfig.AllowOrigins ?? string.Empty;
                appConfig.EnableThumbnail = defConfig.EnableThumbnail;
                appConfig.ThumbnailExts = defConfig.ThumbnailExts ?? string.Empty;
                appConfig.LimitExts = defConfig.LimitExts ?? string.Empty;
            }

            return appConfig;
        }


        /// <summary>
        /// 单个app的配置
        /// </summary>
        public class AppConfig
        {
            /// <summary>
            /// 允许上传的origin， 使用;号隔开，*表示全部
            /// </summary>
            public string AllowOrigins { get; set; }

            /// <summary>
            /// 不允许上传的文件后缀，使用;号隔开
            /// </summary>
            public string LimitExts { get; set; }

            /// <summary>
            /// 是否启用图片缩率图功能
            /// </summary>
            public bool? EnableThumbnail { get; set; }

            /// <summary>
            /// 支持缩率图的后缀，使用;号隔开
            /// </summary>
            public string ThumbnailExts { get; set; }

            public bool IsAllowOrigin(string origin)
            {
                return AllowOrigins == "*" || AllowOrigins.Contains(origin, StringComparison.OrdinalIgnoreCase);
            }
        }
    }
}